<%@include file="/init.jsp"%>

<c:if test="${jockey ne null}">
	<h3>${jockey.name}</h3>

	<div class="row">
		<div class="col-xs-4 col-sm-2">
			<img src="${jockey.portraitUrl}"/>
		</div>
		<div class="col-xs-8 col-sm-10">
			<dl>
				<dt>Id</dt>
				<dd>${jockey.id}</dd>
			</dl>
			<dl>
				<dt>Horse</dt>
				<dd>${jockey.horse}</dd>
			</dl>
		</div>
	</div>

	<portlet:renderURL var="backToOverviewUrl">
       <portlet:param name="mvcPath" value="/jockeys.jsp" />
    </portlet:renderURL>
</c:if>

<a href="${backToOverviewUrl}">Back to overview</a>