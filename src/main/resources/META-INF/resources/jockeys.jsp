<%@include file="/init.jsp"%>

<div class="row">
	<div class="col-md-4">
		<div class="form-group input-select-wrapper">
			<select id="team-select" name="team"></select>
		</div>
	</div>
	<div class="col-md-2">
		<button id="search-button" type="button">Search</button>
	</div>
</div>

<div id="tbl"></div>

<script type="text/javascript">
	AUI().ready("liferay-portlet-url", function(A) {
		$('#search-button').puibutton({
			click: Jockey.reloadTable
		});

		$("#team-select").puidropdown({
			data: Jockey.getTeams
		});

		Jockey.initTable();
	});

	var Jockey = {
		initTable: function() {
			Jockey.getCountAjax().done(function(totalCount) {
				$('#tbl').puidatatable({
					caption: 'Jockeys',
					lazy: true,
					paginator: {
						rows: ${itemsPerPage},
						totalRecords: parseInt(totalCount)
					},
					columns: [
						{field: 'id', headerText: 'Id'},
						{field: 'name', headerText: 'Name'},
						{field: 'horse', headerText: 'Horse'},
						{field: 'teamName', headerText: 'Team name'},
						{field: 'view', headerText: '', content: function(rowData) {
							var detailURL = Liferay.PortletURL.createRenderURL();
							detailURL.setPortletId("${themeDisplay.getPortletDisplay().getId()}");
							detailURL.setParameter("id", rowData.id);
							detailURL.setParameter("mvcRenderCommandName", "/jockey/detail");

							return $('<a href="' + detailURL.toString() + '">View</a>');
						}}
					],
					datasource: Jockey.search
				});
			});
		},

		reloadTable: function(event) {
			Jockey.getCountAjax().done(function(totalCount) {
				$('#tbl').puidatatable("setTotalRecords", parseInt(totalCount));

				// Fix http://jsfiddle.net/ritchiecarroll/9ez9njwd/
				$('#tbl').puidatatable('getPaginator').puipaginator('setPage', 0);
			});
		},

		getCountAjax: function() {
			var countURL = Liferay.PortletURL.createResourceURL();
			countURL.setPortletId("${themeDisplay.getPortletDisplay().getId()}");
			countURL.setParameter("team", $("#team-select").val());
			countURL.setResourceId("/jockey/count");

			return $.ajax({
				type: "GET",
				url: countURL,
				dataType : "text",
				context: this
			});
		},

		getTeams: function(callback) {
			var getTeamsURL = Liferay.PortletURL.createResourceURL();
			getTeamsURL.setPortletId("${themeDisplay.getPortletDisplay().getId()}");
			getTeamsURL.setResourceId("/jockey/get-teams");

			$.ajax({
				type: "GET",
				url: getTeamsURL,
				dataType: "json",
				context: this,
				success: function(teams) {
					callback.call(this, teams);
				}
			});
		},

		search: function(callback, ui) {
			var searchURL = Liferay.PortletURL.createResourceURL();
			searchURL.setPortletId("${themeDisplay.getPortletDisplay().getId()}");
			searchURL.setParameter("team", $("#team-select").val());
			searchURL.setParameter("from", ui.first);
			searchURL.setParameter("to", ui.first + ${itemsPerPage});
			searchURL.setResourceId("/jockey/search");

			$.ajax({
				type: "GET",
				url: searchURL,
				dataType : "json",
				context: this,
				success: function(jockeys) {
					callback.call(this, jockeys);
				}
			});
		}
	}
</script>