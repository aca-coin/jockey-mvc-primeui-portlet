package be.aca.devcon.jockey.mvc.internal.command;

import be.aca.devcon.jockey.service.api.Jockey;
import be.aca.devcon.jockey.service.api.JockeyService;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONSerializer;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@Component(
		immediate = true,
		property = {
				"javax.portlet.name=JockeyMVCPrimeUIPortlet",
				"mvc.command.name=/jockey/search"
		},
		service = MVCResourceCommand.class
)
public class SearchJockeysResourceCommand implements MVCResourceCommand {

	private static final Log LOGGER = LogFactoryUtil.getLog(GetTeamsResourceCommand.class);

	@Reference private JockeyService jockeyService;
	@Reference private JSONFactory jsonFactory;

	public boolean serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws PortletException {
		JSONSerializer serializer = jsonFactory.createJSONSerializer();

		String team = ParamUtil.getString(resourceRequest, "team");
		team = team.isEmpty() ? null : team;

		int from = ParamUtil.getInteger(resourceRequest, "from", QueryUtil.ALL_POS);
		int to = ParamUtil.getInteger(resourceRequest, "to", QueryUtil.ALL_POS);

		List<Jockey> jockeys = jockeyService.getJockeys(team, from, to);

		try {
			PrintWriter writer = resourceResponse.getWriter();
			writer.write(serializer.serialize(jockeys));

			return false;
		} catch (IOException e) {
			LOGGER.error(e);

			return true;
		}
	}
}
