package be.aca.devcon.jockey.mvc.internal.portlet;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import org.osgi.service.component.annotations.Component;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import java.io.IOException;

@Component(
	immediate = true,
	service = Portlet.class,
	property = {
		"com.liferay.portlet.display-category=ACA Devcon",
		"com.liferay.portlet.instanceable=true",
		"com.liferay.portlet.css-class-wrapper=jockey-mvc-primeui-portlet",
		"com.liferay.portlet.header-portlet-css=/css/font-awesome.css",
		"com.liferay.portlet.header-portlet-css=/css/jquery-ui.css",
		"com.liferay.portlet.header-portlet-css=/css/primeui.css",
		"com.liferay.portlet.header-portlet-css=/css/jockey.css",
		"com.liferay.portlet.header-portlet-javascript=/js/jquery-ui.js",
		"com.liferay.portlet.header-portlet-javascript=/js/primeui.js",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/jockeys.jsp",
		"javax.portlet.display-name=Jockey MVC PrimeUI portlet",
		"javax.portlet.security-role-ref=power-user,user",
		"javax.portlet.name=JockeyMVCPrimeUIPortlet"
	}
)
public class JockeyMVCPrimeUIPortlet extends MVCPortlet {

	private static final int ITEMS_PER_PAGE = 3;

	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		renderRequest.setAttribute("itemsPerPage", ITEMS_PER_PAGE);

		super.render(renderRequest, renderResponse);
	}
}
